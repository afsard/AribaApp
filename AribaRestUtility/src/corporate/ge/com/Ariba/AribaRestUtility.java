package corporate.ge.com.Ariba;
 
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.File;
import java.io.InputStream;

import java.net.HttpURLConnection;
import java.net.URL;

import java.net.URLConnection;

import java.nio.file.Files;

import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import sun.misc.IOUtils;


/**
 * This class is used to call Ariba Operations - CREATEPO,CCINVOICE with request payloads as mime attachments.
 * @author Afsar Doodekula
 *
 */
public class AribaRestUtility {
    private static String LINE_FEED = "\r\n";
    
    public AribaRestUtility() {
        super();
    }
    public static String callAribaWithAttachment(String operation, String xml, String attachment, String endpoint, String fileName)
            throws IOException {
        String boundary;
        //final String LINE_FEED = "\r\n";
        HttpURLConnection httpConn;
        OutputStream outputStream;
        PrintWriter writer;
        boundary = Long.toString(System.currentTimeMillis());
        URL url = new URL(endpoint);
        httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setUseCaches(false);
        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        httpConn.setRequestProperty("Content-Type","multipart/related; boundary=" + boundary);
        httpConn.setRequestProperty("User-Agent", "SOA");
        outputStream = httpConn.getOutputStream();
        writer = new PrintWriter(new OutputStreamWriter(outputStream, "UTF-8"),true);

	writer.append("--" + boundary).append(LINE_FEED);
        if(operation.equalsIgnoreCase("CREATEPO"))
            writer.append("Content-Disposition: attachment; filename= po.xml").append(LINE_FEED);
        else if(operation.equalsIgnoreCase("CCINVOICE"))
            writer.append("Content-Disposition: attachment; filename= ccinvoice.xml").append(LINE_FEED);
        writer.append("Content-Type: text/xml; charset= UTF-8").append(LINE_FEED);
        writer.append(LINE_FEED); 
        writer.append(xml); 
        writer.append(LINE_FEED);
        writer.flush();
        if(attachment.length() > 0 & operation.equalsIgnoreCase("CREATEPO")) {
           byte[] bytes = attachment.getBytes();
			writer.append("--" + boundary).append(LINE_FEED);
			writer.append("Content-Disposition: attachment; filename=").append(fileName).append(LINE_FEED);
			writer.append("Content-Type: application/pdf").append(LINE_FEED);
                        writer.append("Content-Transfer-Encoding: base64").append(LINE_FEED);
			writer.append(LINE_FEED); 
			//writer.append(attachment); 
			writer.flush();
			outputStream.write(bytes);
                        outputStream.flush();
			writer.append(LINE_FEED);
                        
        }   
        else if(attachment.length() > 0 & operation.equalsIgnoreCase("CCINVOICE")){
            writer.append("--" + boundary).append(LINE_FEED);
            writer.append("Content-Disposition: attachment; filename=ccInvoiceAttachment.xml").append(LINE_FEED);
            writer.append("Content-Type: text/xml; charset= UTF-8").append(LINE_FEED);
            writer.append("Content-ID: <payload-TT@sap.com>").append(LINE_FEED);
            writer.append(LINE_FEED); 
            writer.append(attachment); 
            writer.append(LINE_FEED);
            writer.flush();
        }
        writer.append("--" + boundary + "--").append(LINE_FEED);
        writer.append(LINE_FEED).flush();
        writer.flush();
        
        System.out.println("writer:"+outputStream.toString());
        writer.close();
        List<String> response = new ArrayList<String>();
        int status = httpConn.getResponseCode();
        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
            httpConn.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                response.add(line);
            }
            reader.close();
            httpConn.disconnect();
        } else {
            throw new IOException("Ariba returned error: " + status);
        }
        System.out.println("response"+ response.toString());
        StringBuilder sb = new StringBuilder();
        for (String s : response)
        {
            sb.append(s);
            sb.append(LINE_FEED);
        }
        return sb.toString();
    }
    
    // trusting all certificate 
    public static void doTrustToCertificates() throws Exception {
           Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
           TrustManager[] trustAllCerts = new TrustManager[]{
                   new X509TrustManager() {
                       public X509Certificate[] getAcceptedIssuers() {
                           return null;
                       }

                       public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                           return;
                       }

                       public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                           return;
                       }
                   }
           };

           SSLContext sc = SSLContext.getInstance("SSL");
           sc.init(null, trustAllCerts, new SecureRandom());
           HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
           HostnameVerifier hv = new HostnameVerifier() {
               public boolean verify(String urlHostName, SSLSession session) {
                   if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
                       System.out.println("Warning: URL host '" + urlHostName + "' is different to SSLSession host '" + session.getPeerHost() + "'.");
                   }
                   return true;
               }
           };
           HttpsURLConnection.setDefaultHostnameVerifier(hv);
       }
    
    public static String callAribaWithZipAttachment(String file1name,String file2name,String file3name,  String file1Content,String file2Content,String file3Content,String endpoint)
            throws IOException,Exception {
        String zipFileName =  "INCR_"+ new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date())+".zip";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);
        if(file1name.length()>1) {
            ZipEntry entry = new ZipEntry(file1name); 
            zos.putNextEntry(entry);
            zos.write(file1Content.getBytes());
        }
        if(file2name.length()>1) {
            ZipEntry entry = new ZipEntry(file2name); 
            zos.putNextEntry(entry);
            zos.write(file2Content.getBytes());
        }
        if(file3name.length()>1) {
            ZipEntry entry = new ZipEntry(file3name); 
            zos.putNextEntry(entry);
            zos.write(file3Content.getBytes());
        }
         zos.closeEntry();
         //File f = baos.toByteArray().
        String boundary = "--"+Long.toHexString(System.currentTimeMillis());
//        doTrustToCertificates();
        OutputStream outputStream;
        URL url = new URL(endpoint);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setUseCaches(false);
        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        httpConn.setRequestProperty("Content-Type","multipart/form-data; boundary=" + boundary);
        //File file = new File("C:/docs/WBS2018081320180158200001.zip");
        //System.out.println("file"+ file.toString());
        PrintWriter writer = null;
        try {
                   outputStream = httpConn.getOutputStream();
                   writer = new PrintWriter(new OutputStreamWriter(httpConn.getOutputStream(), "UTF-8"));
            
                   writer.append("--" + boundary).append(LINE_FEED);
                   writer.append("Content-Disposition: form-data; name=\"fullload\"").append(LINE_FEED).append(LINE_FEED);
                   writer.append("false").append(LINE_FEED).flush();
                   
                   writer.append("--" + boundary).append(LINE_FEED);
                   writer.append("Content-Disposition: form-data; name=\"sharedsecret\"").append(LINE_FEED).append(LINE_FEED);
                   writer.append("ariba123").append(LINE_FEED).flush();
                
                   writer.append("--" + boundary).append(LINE_FEED);
                   writer.append("Content-Disposition: form-data; name=\"clienttype\"").append(LINE_FEED).append(LINE_FEED);
                   writer.append("Test").append(LINE_FEED).flush();
                    
                   writer.append("--" + boundary).append(LINE_FEED);
                   writer.append("Content-Disposition: form-data; name=\"clientinfo\"").append(LINE_FEED).append(LINE_FEED);
                   writer.append("Master Data Upload Form").append(LINE_FEED).flush();
            
                   writer.append("--" + boundary).append(LINE_FEED);
                   writer.append("Content-Disposition: form-data; name=\"clientversion\"").append(LINE_FEED).append(LINE_FEED);
                   writer.append("1.0").append(LINE_FEED).flush();
            
                   writer.append("--" + boundary).append(LINE_FEED);
                   writer.append("Content-Disposition: form-data; name=\"event\"").append(LINE_FEED).append(LINE_FEED);
                   writer.append("Import Batch Data").append(LINE_FEED).flush();
            
                   
                   writer.append("--" + boundary).append(LINE_FEED);
                   writer.append("Content-Disposition: form-data; name=\"content\"; filename=\"" + zipFileName + "\"").append(LINE_FEED);
                   writer.append("Content-Type: application/zip").append(LINE_FEED).append(LINE_FEED).flush();
            
                   
                  /* byte[] buffer = new byte[4096];
                   int bytesRead = -1;
                   while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                   }*/
                   baos.writeTo(outputStream);
                   outputStream.flush();
                  // inputStream.close();
                   writer.flush(); 
                   writer.append(LINE_FEED);
                   writer.append("--" + boundary + "--").append(LINE_FEED);
                   writer.append(LINE_FEED);
                   writer.flush();
                   System.out.println("writer:"+outputStream.toString());
                   writer.close();
               } catch (Exception e) {
                   System.out.append("Exception writing file" + e);
               } finally {
                   if (writer != null) writer.close();
               }
        List<String> response = new ArrayList<String>();
        int status = httpConn.getResponseCode();
        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                httpConn.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                response.add(line);
            }
            reader.close();
            httpConn.disconnect();
        } else {
            InputStream in = httpConn.getErrorStream();
            int ch;
            StringBuilder sb = new StringBuilder();
            while((ch = in.read()) != -1)
                sb.append((char)ch);
            
            
            throw new Exception("Ariba returned error: " + sb.toString()+ httpConn.getResponseCode()+httpConn.getResponseMessage());
        }
        System.out.println("response"+ response.toString());
        StringBuilder sb = new StringBuilder();
        for (String s : response)
        {
            sb.append(s);
            sb.append(LINE_FEED);
        }
        return sb.toString();
        
    }
    
    public static void main(String args[]) {
        String xml = "<test>test xml</test>";
        String endpoint = "https://service-2.ariba.com/service/transaction/cxml.asp";
        String attachment = "sample attachment";
        String fileName = "PO.PDF";
        System.out.println("starting program");
        try {
            //String s  = AribaRestUtility.callAribaWithAttachment("CREATEPO",xml, attachment, endpoint, fileName);
            String s  = AribaRestUtility.callAribaWithZipAttachment("test.csv",xml,"","","","", "\"https://s1-integration.ariba.com/Buyer/fileupload?realm=GEPowerChild1Dev-T\"");
            System.out.println("response"+s);
        }
        catch (Exception e) {
            System.out.println("Exception-"+e);
        }
    }
}